﻿using Urho;

namespace DicePhysics.Desktop
{
    class Program
    {
        static void Main(string[] args)
        {
            ApplicationOptions opts = new ApplicationOptions("Data");

            opts.Width = 800;
            opts.Height = 600;
            opts.ResizableWindow = true;

            new MyGame(opts).Run();
        }
    }
}
