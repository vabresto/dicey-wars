﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DicePhysics
{
    class AIAgentBase : Player
    {
        public AIAgentBase(PlayerColour colour, int id)
            : base(colour, id)
        {
            IsLocalPlayer = false;
            IsHuman = false;
        }
    }
}
