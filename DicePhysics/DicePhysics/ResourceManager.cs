﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Urho;
using Urho.Gui;
using Urho.Actions;
using Urho.Shapes;
using Urho.Physics;
using Urho.IO;

namespace DicePhysics
{
    public enum PlayerColour
    {
        Red,
        Green,
        Magenta,
        Blue,
    }

    public enum MaterialResourceType
    {
        Default,
        Highlight,
        Select,

        Dice,
        Token,
    }

    public enum ModelResourceType
    {
        Floor,
        Token,
        Dice,
    }

    class ResourceManager
    {
        private static Dictionary<Tuple<PlayerColour, MaterialResourceType>, Material> _playerMaterialsList =
            new Dictionary<Tuple<PlayerColour, MaterialResourceType>, Material>();

        private static Dictionary<ModelResourceType, Model> _modelsList = new Dictionary<ModelResourceType, Model>();

        private static bool isInitialized = false;

        public static void Initialize(Urho.Resources.ResourceCache cache)
        {
            //Models
            _modelsList.Add(ModelResourceType.Floor, cache.GetModel("Models/Box.mdl"));
            _modelsList.Add(ModelResourceType.Token, cache.GetModel("Models/Box.mdl"));
            _modelsList.Add(ModelResourceType.Dice, cache.GetModel("Models/Cube.mdl"));

            //Materials
            // Red
            _playerMaterialsList.Add(
                new Tuple<PlayerColour, MaterialResourceType>(PlayerColour.Red, MaterialResourceType.Default),
                cache.GetMaterial("Materials/Red/RedDefault.xml")
                );
            _playerMaterialsList.Add(
                new Tuple<PlayerColour, MaterialResourceType>(PlayerColour.Red, MaterialResourceType.Highlight),
                cache.GetMaterial("Materials/Red/RedHighlight.xml")
                );
            _playerMaterialsList.Add(
                new Tuple<PlayerColour, MaterialResourceType>(PlayerColour.Red, MaterialResourceType.Select),
                cache.GetMaterial("Materials/Red/RedSelect.xml")
                );
            _playerMaterialsList.Add(
               new Tuple<PlayerColour, MaterialResourceType>(PlayerColour.Red, MaterialResourceType.Dice),
               cache.GetMaterial("Materials/Red/RedDice.xml")
               );
            _playerMaterialsList.Add(
                new Tuple<PlayerColour, MaterialResourceType>(PlayerColour.Red, MaterialResourceType.Token),
                cache.GetMaterial("Materials/BoxMaterial.xml")
                );

            // Green
            _playerMaterialsList.Add(
                new Tuple<PlayerColour, MaterialResourceType>(PlayerColour.Green, MaterialResourceType.Default),
                cache.GetMaterial("Materials/Green/GreenDefault.xml")
                );
            _playerMaterialsList.Add(
                new Tuple<PlayerColour, MaterialResourceType>(PlayerColour.Green, MaterialResourceType.Highlight),
                cache.GetMaterial("Materials/Green/GreenHighlight.xml")
                );
            _playerMaterialsList.Add(
                new Tuple<PlayerColour, MaterialResourceType>(PlayerColour.Green, MaterialResourceType.Select),
                cache.GetMaterial("Materials/Green/GreenSelect.xml")
                );
            _playerMaterialsList.Add(
               new Tuple<PlayerColour, MaterialResourceType>(PlayerColour.Green, MaterialResourceType.Dice),
               cache.GetMaterial("Materials/Green/GreenDice.xml")
               );
            _playerMaterialsList.Add(
                new Tuple<PlayerColour, MaterialResourceType>(PlayerColour.Green, MaterialResourceType.Token),
                cache.GetMaterial("Materials/BoxMaterial.xml")
                );

            // Magenta
            _playerMaterialsList.Add(
                new Tuple<PlayerColour, MaterialResourceType>(PlayerColour.Magenta, MaterialResourceType.Default),
                cache.GetMaterial("Materials/Magenta/MagentaDefault.xml")
                );
            _playerMaterialsList.Add(
                new Tuple<PlayerColour, MaterialResourceType>(PlayerColour.Magenta, MaterialResourceType.Highlight),
                cache.GetMaterial("Materials/Magenta/MagentaHighlight.xml")
                );
            _playerMaterialsList.Add(
                new Tuple<PlayerColour, MaterialResourceType>(PlayerColour.Magenta, MaterialResourceType.Select),
                cache.GetMaterial("Materials/Magenta/MagentaSelect.xml")
                );
            _playerMaterialsList.Add(
               new Tuple<PlayerColour, MaterialResourceType>(PlayerColour.Magenta, MaterialResourceType.Dice),
               cache.GetMaterial("Materials/Magenta/MagentaDice.xml")
               );
            _playerMaterialsList.Add(
                new Tuple<PlayerColour, MaterialResourceType>(PlayerColour.Magenta, MaterialResourceType.Token),
                cache.GetMaterial("Materials/BoxMaterial.xml")
                );

            // Blue
            _playerMaterialsList.Add(
                new Tuple<PlayerColour, MaterialResourceType>(PlayerColour.Blue, MaterialResourceType.Default),
                cache.GetMaterial("Materials/Blue/BlueDefault.xml")
                );
            _playerMaterialsList.Add(
                new Tuple<PlayerColour, MaterialResourceType>(PlayerColour.Blue, MaterialResourceType.Highlight),
                cache.GetMaterial("Materials/Blue/BlueHighlight.xml")
                );
            _playerMaterialsList.Add(
                new Tuple<PlayerColour, MaterialResourceType>(PlayerColour.Blue, MaterialResourceType.Select),
                cache.GetMaterial("Materials/Blue/BlueSelect.xml")
                );
            _playerMaterialsList.Add(
               new Tuple<PlayerColour, MaterialResourceType>(PlayerColour.Blue, MaterialResourceType.Dice),
               cache.GetMaterial("Materials/Blue/BlueDice.xml")
               );
            _playerMaterialsList.Add(
                new Tuple<PlayerColour, MaterialResourceType>(PlayerColour.Blue, MaterialResourceType.Token),
                cache.GetMaterial("Materials/BoxMaterial.xml")
                );

            isInitialized = true;
        }

        public static Material GetMaterial(Tuple<PlayerColour, MaterialResourceType> key)
        {
            if (!isInitialized)
            {
                throw new System.NullReferenceException("ResourceManager hasn't been initialized yet!");
            }

            return _playerMaterialsList[key];
        }

        public static Model GetModel(ModelResourceType key)
        {
            if (!isInitialized)
            {
                throw new System.NullReferenceException("ResourceManager hasn't been initialized yet!");
            }

            return _modelsList[key];
        }
    }
}
