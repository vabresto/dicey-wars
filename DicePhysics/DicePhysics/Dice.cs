﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Urho;
using Urho.Gui;
using Urho.Actions;
using Urho.Shapes;
using Urho.Physics;
using Urho.IO;

namespace DicePhysics
{
    public class Dice
    {
        public Node Obj
        { get; set; }

        public static int NumDice = 0;

        public Dice(Scene scene, Urho.Resources.ResourceCache cache)
        {
            Log.Write(LogLevel.Debug, "Starting...");

            Obj = scene.CreateChild(name: "Box" + NumDice);
            ++NumDice;
            Obj.SetScale(1f);

            Obj.AddTag("Selectable");
            Obj.AddTag("Die");

            StaticModel boxModel = Obj.CreateComponent<StaticModel>();
            boxModel.Model = cache.GetModel("Models/Box.mdl");
            boxModel.SetMaterial(cache.GetMaterial("Materials/BoxMaterial.xml"));

            RigidBody boxBody = Obj.CreateComponent<RigidBody>();
            boxBody.Mass = 5f;

            //CollisionShape boxShape = Obj.CreateComponent<CollisionShape>();
            //boxShape.SetBox(Obj.Scale, Vector3.Zero, Quaternion.Identity);

            ResetCube();
        }

        public void ResetCube()
        {
            Random rand = MyGame.rand;

            if (Obj != null)
            {
                Obj.Position = new Vector3(x: rand.Next() % 10 - 5, y: rand.Next() % 8 + 2, z: rand.Next() % 10 -5 );
                Obj.Rotation = new Quaternion(x: rand.Next() % 90, y: rand.Next() % 90, z: rand.Next() % 90);
                Obj.GetComponent<RigidBody>().SetLinearVelocity(new Vector3(rand.Next() % 6 - 3, rand.Next() % 6 - 3, rand.Next() % 6 - 3));
                Obj.GetComponent<RigidBody>().SetAngularVelocity(new Vector3(rand.Next() % 10 -5, rand.Next() % 10 - 5, rand.Next() % 10 - 5));
            }
        }
    }
}
