﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Urho;
using Urho.Gui;
using Urho.Actions;
using Urho.Shapes;
using Urho.Physics;
using Urho.IO;

namespace DicePhysics
{
    public class MyGame : Application
    {
        [Preserve]
        public MyGame(ApplicationOptions opts) : base(opts) {}

        public static Scene scene;

        public static List<Dice> dice;
        Queue<float> lastFPS = new Queue<float>();
        public static HexagonManager hexManager;
        public static GameManager gameManager;
        public static PhysicalCombat combat = new PhysicalCombat();

        public Text diceValueCounterText;
        public Text GameOverText;

        float timeSinceLastReset = 0;
        const float minTimeBetweenResets = 1f;

        public static Random rand;

        public bool IsInitialized = false;

        bool wasMouseDownLastFrame = false;

        static MyGame()
        {
            UnhandledException += (s, e) =>
            {
                if (Debugger.IsAttached)
                {
                    //
                    Log.Write(LogLevel.Error, e.ToString());
                    Debugger.Break();
                }
                e.Handled = true;
            };
        }
        
        /*
        private void SplashScreen()
        {
            Log.Write(LogLevel.Debug, "Splash!");
            BorderImage splashUI = new BorderImage(Context);
            splashUI.Name = "Splash";
            Texture texture = ResourceCache.GetTexture2D("Textures/xamarin.png");
            splashUI.Texture = texture;
            splashUI.HorizontalAlignment = HorizontalAlignment.Center;
            splashUI.VerticalAlignment = VerticalAlignment.Center;
            UI.Root.AddChild(splashUI);
            //UI.Render();
            Engine.RunFrame();
        }

        private void HandleSplash()
        {
            var splashUI = UI.Root.GetChild("Splash", true);
            if (splashUI != null)
            {
                splashUI.Remove();
                Log.Write(LogLevel.Debug, "End Splash!");
            }
        }
        */

        protected override void Start()
        {
            Log.Write(LogLevel.Debug, "Starting...");
            Graphics.WindowTitle = "Dicey Wars";

            ResourceManager.Initialize(ResourceCache);
            //SplashScreen();

            gameManager = new GameManager(4);

            rand = new Random();

            if (Platform != Platforms.Android && Platform != Platforms.iOS)
            {
                Input.SetMouseVisible(true, false);
            }

            Log.Write(LogLevel.Debug, "Creating scene...");
            CreateScene();

            // Subscribe to Esc key:
            Input.SubscribeToKeyDown(args => { if (args.Key == Key.Esc) Exit(); });
            Input.SubscribeToTouchMove(DoTouchCameraMove);
            Input.SubscribeToTouchEnd(OnTouchEnd);

            Input.SubscribeToGestureInput(OnPinch);

            Engine.SubscribeToPostRenderUpdate(args => TestDebug());
            IsInitialized = true;
            //HandleSplash();
        }

        private void OnPinch(GestureInputEventArgs args)
        {
            Log.Write(LogLevel.Info, "Pinch?");
        }

        void TestDebug()
        {
            if (combat.IsInCombat)
            {
                combat.DrawDebug();
            }
        }

        protected override void OnUpdate(float timeStep)
        {
            base.OnUpdate(timeStep);
            if (!IsInitialized)
            {
                return;
            }

            timeSinceLastReset += timeStep;
            

            DoFPS(timeStep);
            hexManager.OnUpdate(timeStep);

            if (!Platform.IsMobile())
            {
                if (Input.GetMouseButtonDown(MouseButton.Left))
                {
                    DoCameraMove(timeStep);
                }

                if (!Input.GetMouseButtonDown(MouseButton.Left) && wasMouseDownLastFrame/*|| Input.NumTouches > 0*/)
                {
                    //Raycast();

                    IntVector2 pos = Input.MousePosition;

                    float x = (float)pos.X / (float)Graphics.Width;
                    float y = (float)pos.Y / (float)Graphics.Height;

                    Ray camRay = scene.GetChild("camera").GetComponent<Camera>().GetScreenRay(x, y);
                    RayQueryResult? results = scene.GetComponent<Octree>().RaycastSingle(camRay);
                    if (results.HasValue)
                    {
                        if (results.Value.Node.HasTag("Selectable"))
                        {
                            hexManager.OnTap(new Vector2(results.Value.Node.Position.X, results.Value.Node.Position.Z));
                        }
                    }
                    combat.OnTap();
                }

                if (Input.GetMouseButtonDown(MouseButton.Left))
                {
                    wasMouseDownLastFrame = true;
                }
                else
                {
                    wasMouseDownLastFrame = false;
                }
            }
            
            if (combat.IsInCombat)
            {
                diceValueCounterText.Value = "Player 1: ";
                diceValueCounterText.Value += combat.player1Score;
                diceValueCounterText.Value += "\nPlayer 2: ";
                diceValueCounterText.Value += combat.player2Score;

                if (combat.DoneCombat())
                {
                    combat.EndCombat();
                    dice.Clear();           // Clears all 3D/simulated-ly thrown dice
                    diceValueCounterText.Value = "";
                }
            }
            

            Node lightNode = scene.GetChild("light");
            Node camNode = scene.GetChild("camera");

            lightNode.Position = new Vector3(camNode.Position.X, lightNode.Position.Y, camNode.Position.Z);

            if (!combat.IsInCombat)
            {
                if (!PlayerManager.IsPlayerDoneTurn())
                {
                    PlayerManager.PlayerOnUpdate(timeStep);
                    PlayerManager.PlayerMakeMove();
                }
                else
                {
                    gameManager.NextPlayersTurn();
                }
            }

            if (gameManager.IsGameOver())
            {
                GameOverText.Value = string.Format("Game Over!\nPlayer {0} wins!", gameManager.winnerPlayer);
            }
        }
        
        private void OnTouchEnd(TouchEndEventArgs args)
        {
            IntVector2 pos = new IntVector2(args.X, args.Y);

            float x = (float)pos.X / (float)Graphics.Width;
            float y = (float)pos.Y / (float)Graphics.Height;

            Ray camRay = scene.GetChild("camera").GetComponent<Camera>().GetScreenRay(x, y);
            RayQueryResult? results = scene.GetComponent<Octree>().RaycastSingle(camRay);
            if (results.HasValue)
            {
                if (results.Value.Node.HasTag("Selectable"))
                {
                    hexManager.OnTap(new Vector2(results.Value.Node.Position.X, results.Value.Node.Position.Z));
                }
            }

            combat.OnTap();
        }

        private void DoTouchCameraMove(TouchMoveEventArgs args)
        {
            //Pan
            if (Input.NumTouches == 1)
            {
                Node camNode = scene.GetChild("camera");
                Camera cam = camNode.GetComponent<Camera>();
                camNode.Translate(new Vector3(-args.DX / 5, 0, args.DY / 5), TransformSpace.World);
            }

            //Zoom
            if (Input.NumTouches == 2)
            {
                Node camNode = scene.GetChild("camera");
                Camera cam = camNode.GetComponent<Camera>();

                IntVector2 oldDelta = Input.GetTouch(0).LastPosition - Input.GetTouch(1).LastPosition;
                IntVector2 newDelta = Input.GetTouch(0).Position - Input.GetTouch(1).Position;

                if (oldDelta.LengthSquared < newDelta.LengthSquared)
                {
                    //Zooming in
                    if (camNode.Position.Y > 15)
                    {
                        camNode.Translate(new Vector3(0, -1, 0), TransformSpace.World);
                    }
                }
                else
                {
                    //Zooming out
                    if (camNode.Position.Y < 80)
                    {
                        camNode.Translate(new Vector3(0, 1, 0), TransformSpace.World);
                    }
                }
            }
            
        }

        private void DoCameraMove(float timeStep)
        {
            IntVector2 mouseMove = Input.MouseMove;

            Node camNode = scene.GetChild("camera");
            camNode.Translate(new Vector3(-mouseMove.X, 0, mouseMove.Y), TransformSpace.World);
        }

        private void DoFPS(float timeStep)
        {
            if (lastFPS.Count >= 60)
            {
                lastFPS.Dequeue();
            }
            lastFPS.Enqueue(1f / timeStep);
            float avg = 0f;
            foreach (float _fps in lastFPS)
            {
                avg += _fps;
            }
            avg /= lastFPS.Count;

            Text fps = (Text)UI.Root.GetChild(1);
            fps.Value = "Instant FPS: ";
            fps.Value += (1f / timeStep).ToString("0");
            fps.Value += "\nAverage FPS: ";
            fps.Value += avg.ToString("0");
            fps.Value += "\nDice: ";
            //fps.Value += dice.Count;  // <-- refers to 3d dice
            fps.Value += hexManager.CalcTotalNumberOfDiceOwned(gameManager.GetCurrentPlayer());
            fps.Value += "\nTurn: Player ";
            fps.Value += gameManager.GetCurrentPlayer();
            fps.Value += "\nOwned Hexes: ";
            fps.Value += hexManager.ownedTilesList[gameManager.GetCurrentPlayer()].Count;
            fps.Value += "\nLongest Chain: ";
            fps.Value += hexManager.CalcLongestConnectedChainForPlayer(gameManager.GetCurrentPlayer());
            fps.Value += "\nSimulating Dice: ";
            fps.Value += combat.SimulateRealDice ? "True" : "False";
        }

        public static void FocusCamera(Vector3 focusPos)
        {
            scene.GetChild("camera").Position = new Vector3(focusPos.X, focusPos.Y + 35f, focusPos.Z - 25f);
        }

        void Raycast()
        {
            /*
            IntVector2 pos = Input.MousePosition;

            if (Input.NumTouches > 0)
            {
                pos = Input.GetTouch(0).Position;
            }

            float x = (float)pos.X / (float)Graphics.Width;
            float y = (float)pos.Y / (float)Graphics.Height;

            Ray camRay = scene.GetChild("camera").GetComponent<Camera>().GetScreenRay(x, y);
            RayQueryResult? results = scene.GetComponent<Octree>().RaycastSingle(camRay);
            if (results.HasValue)
            {
                if (results.Value.Node.HasTag("Selectable"))
                {
                    Log.Write(LogLevel.Info, string.Format("Selected node ({0}, {1})!", results.Value.Node.Position.X, results.Value.Node.Position.Z));

                    Vector2 mapPos = new Vector2(results.Value.Node.Position.X, results.Value.Node.Position.Z);
                    AxialCoord location = AxialCoord.Round(new AxialCoord(mapPos));

                    Log.Write(LogLevel.Info, string.Format("Node has coordinates ({0}, {1})!", location.Q, location.R));

                    var hex = hexManager.GetHex(location);

                    if (hex != null)
                    {
                        Log.Write(LogLevel.Info, "Found a hex!");
                        hex.Obj.GetComponent<StaticModel>().SetMaterial(ResourceCache.GetMaterial("Materials/HexagonMaterialSelected.xml"));
                    }
                }
            }
            */
        }

        void HandleSpawnButtonReleased(ReleasedEventArgs args)
        {
            //dice.Add(new Dice(scene, ResourceCache));
        }

        void HandleResetButtonReleased(ReleasedEventArgs args)
        {
            if (timeSinceLastReset >= minTimeBetweenResets)
            {
                timeSinceLastReset = 0f;
                foreach (Dice die in dice)
                {
                    if (die != null)
                    {
                        die.ResetCube();
                    }
                }
            }
        }

        void HandleSimulateButtonReleased(ReleasedEventArgs args)
        {
            combat.SimulateRealDice = !combat.SimulateRealDice;
        }

        void HandleNextTurnButtonReleased(ReleasedEventArgs args)
        {
            PlayerManager.LocalPlayerEndedTurn();
        }

        void CreateScene()
        {
            dice = new List<Dice>();

            // UI text 
            var helloText = new Text(Context);
            helloText.Value = "Dicey Wars";
            helloText.HorizontalAlignment = HorizontalAlignment.Center;
            helloText.VerticalAlignment = VerticalAlignment.Top;
            helloText.SetColor(new Color(r: 0f, g: 1f, b: 1f));
            helloText.SetFont(font: ResourceCache.GetFont("Fonts/Font.ttf"), size: 30);
            UI.Root.AddChild(helloText);

            Text fpsText = new Text(Context);
            fpsText.HorizontalAlignment = HorizontalAlignment.Left;
            fpsText.VerticalAlignment = VerticalAlignment.Top;
            fpsText.SetColor(new Color(r: 0f, g: 1f, b: 0f));
            fpsText.SetFont(font: ResourceCache.GetFont("Fonts/Font.ttf"), size: 18);
            UI.Root.AddChild(fpsText);

            diceValueCounterText = new Text(Context);
            diceValueCounterText.HorizontalAlignment = HorizontalAlignment.Right;
            diceValueCounterText.VerticalAlignment = VerticalAlignment.Top;
            diceValueCounterText.SetColor(new Color(r: 1f, g: 0f, b: 1f));
            diceValueCounterText.SetFont(font: ResourceCache.GetFont("Fonts/Font.ttf"), size: 18);
            UI.Root.AddChild(diceValueCounterText);

            GameOverText = new Text(Context);
            GameOverText.Value = "";
            GameOverText.HorizontalAlignment = HorizontalAlignment.Center;
            GameOverText.VerticalAlignment = VerticalAlignment.Center;
            GameOverText.SetColor(new Color(r: 1f, g: 0f, b: 0f));
            GameOverText.SetFont(font: ResourceCache.GetFont("Fonts/Font.ttf"), size: 45);
            UI.Root.AddChild(GameOverText);

            // Spawn dice button
            //Button spawnDie = new Button();
            //spawnDie.Name = "Spawn Die";
            //spawnDie.HorizontalAlignment = HorizontalAlignment.Right;
            //spawnDie.VerticalAlignment = VerticalAlignment.Bottom;
            //spawnDie.SetSize(200, 50);

            //spawnDie.SubscribeToReleased(HandleSpawnButtonReleased);

            //Text buttonSpawnText = new Text();
            //spawnDie.AddChild(buttonSpawnText);
            //buttonSpawnText.HorizontalAlignment = HorizontalAlignment.Center;
            //buttonSpawnText.VerticalAlignment = VerticalAlignment.Center;
            //buttonSpawnText.Value = "Spawn another die";
            //buttonSpawnText.SetColor(new Color(r: 0f, g: 0f, b: 3f));
            //buttonSpawnText.SetFont(font: ResourceCache.GetFont("Fonts/Font.ttf"), size: 15);

            //UI.Root.AddChild(spawnDie);

            // Reset dice button
            //Button resetDice = new Button();
            //resetDice.Name = "Reset Dice";
            //resetDice.HorizontalAlignment = HorizontalAlignment.Left;
            //resetDice.VerticalAlignment = VerticalAlignment.Bottom;
            //resetDice.SetSize(200, 50);

            //resetDice.SubscribeToReleased(HandleResetButtonReleased);

            //Text buttonResetText = new Text();
            //resetDice.AddChild(buttonResetText);
            //buttonResetText.HorizontalAlignment = HorizontalAlignment.Center;
            //buttonResetText.VerticalAlignment = VerticalAlignment.Center;
            //buttonResetText.Value = "Reset dice";
            //buttonResetText.SetColor(new Color(r: 0f, g: 0f, b: 3f));
            //buttonResetText.SetFont(font: ResourceCache.GetFont("Fonts/Font.ttf"), size: 15);

            //UI.Root.AddChild(resetDice);

            // Toggle Simulate Dice
            Button simulateDice = new Button();
            simulateDice.Name = "Simulate Dice";
            simulateDice.HorizontalAlignment = HorizontalAlignment.Left;
            simulateDice.VerticalAlignment = VerticalAlignment.Bottom;
            simulateDice.SetSize(250, 50);

            simulateDice.SubscribeToReleased(HandleSimulateButtonReleased);

            Text buttonSimulateText = new Text();
            simulateDice.AddChild(buttonSimulateText);
            buttonSimulateText.HorizontalAlignment = HorizontalAlignment.Center;
            buttonSimulateText.VerticalAlignment = VerticalAlignment.Center;
            buttonSimulateText.Value = "Toggle Simulate dice";
            buttonSimulateText.SetColor(new Color(r: 0f, g: 0f, b: 3f));
            buttonSimulateText.SetFont(font: ResourceCache.GetFont("Fonts/Font.ttf"), size: 15);

            UI.Root.AddChild(simulateDice);

            // Next player's turn button
            Button nextPlayerTurn = new Button();
            nextPlayerTurn.Name = "Reset Dice";
            nextPlayerTurn.HorizontalAlignment = HorizontalAlignment.Center;
            nextPlayerTurn.VerticalAlignment = VerticalAlignment.Bottom;
            nextPlayerTurn.SetSize(200, 50);

            nextPlayerTurn.SubscribeToReleased(HandleNextTurnButtonReleased);

            Text nextPlayerTurnText = new Text();
            nextPlayerTurn.AddChild(nextPlayerTurnText);
            nextPlayerTurnText.HorizontalAlignment = HorizontalAlignment.Center;
            nextPlayerTurnText.VerticalAlignment = VerticalAlignment.Center;
            nextPlayerTurnText.Value = "End Turn";
            nextPlayerTurnText.SetColor(new Color(r: 0f, g: 0f, b: 3f));
            nextPlayerTurnText.SetFont(font: ResourceCache.GetFont("Fonts/Font.ttf"), size: 15);

            UI.Root.AddChild(nextPlayerTurn);

            // 3D scene with Octree
            scene = new Scene(Context);
            scene.CreateComponent<Octree>();
            scene.CreateComponent<PhysicsWorld>();
            scene.CreateComponent<DebugRenderer>();

            // Box
            //dice.Add(new Dice(scene, ResourceCache));

            //Plane
            /*
            Node planeNode = scene.CreateChild(name: "Plane");
            planeNode.Position = new Vector3(x: 0, y: 0, z: 0);
            planeNode.Scale = new Vector3(x: 100, y: 1, z: 100);
            StaticModel planeModel = planeNode.CreateComponent<StaticModel>();
            planeModel.Model = ResourceCache.GetModel("Models/Box.mdl");
            //planeModel.SetMaterial(ResourceCache.GetMaterial("Materials/PlaneMaterial.xml"));
            RigidBody planeBody = planeNode.CreateComponent<RigidBody>();
            planeBody.Friction = 0.8f;
            CollisionShape planeShape = planeNode.CreateComponent<CollisionShape>();
            planeShape.SetBox(planeNode.Scale, Vector3.Zero, Quaternion.Identity);

            Node background = scene.CreateChild(name: "background");
            background.Position = new Vector3(x: 0, y: -5, z: 0);
            background.Scale = new Vector3(x: 100, y: 1, z: 100);
            StaticModel backgroundModel = background.CreateComponent<StaticModel>();
            backgroundModel.Model = ResourceCache.GetModel("Models/Box.mdl");
            backgroundModel.SetMaterial(ResourceCache.GetMaterial("Materials/BoxMaterial.xml"));
            */

            /*
            Node tile = scene.CreateChild(name: "tile");
            tile.Position = new Vector3(x: 0, y: 2, z: 0);
            tile.Scale = new Vector3(x: 2, y: 2, z: 2);
            tile.Rotation = new Quaternion(x: 0, y: 90, z: -90);
            StaticModel tileModel = tile.CreateComponent<StaticModel>();
            tileModel.Model = ResourceCache.GetModel("Models/Hexagon.mdl");
            tileModel.SetMaterial(ResourceCache.GetMaterial("Materials/HexagonMaterial.xml"));
            */

            hexManager = new HexagonManager(scene, ResourceCache);

            // Light
            /*for (int cntx = -3; cntx <= 3; ++cntx)
            {
                for (int cnty = -4; cnty <= 4; ++cnty)
                {
                    Node lightNode = scene.CreateChild();
                    var light = lightNode.CreateComponent<Light>();
                    light.Range = 50;
                    light.Brightness = 0.4f;
                    lightNode.Position = new Vector3(x: cntx * 30f, y: 15f, z: cnty * 30f);
                }
            }*/
            Node lightNode = scene.CreateChild("light");
            var light = lightNode.CreateComponent<Light>();
            light.Range = 50;
            light.Brightness = 0.2f;
            lightNode.Position = new Vector3(x: 30f, y: 15f, z: 30f);

            var zoneNode = scene.CreateChild("zone");
            var zone = zoneNode.CreateComponent<Zone>();

            // Set same volume as the Octree, set a close bluish fog and some ambient light
            zone.SetBoundingBox(new BoundingBox(-1000.0f, 1000.0f));
            zone.AmbientColor = new Color(0.8f, 0.8f, 0.8f);


            // Camera
            Node cameraNode = scene.CreateChild(name: "camera");
            Camera camera = cameraNode.CreateComponent<Camera>();
            //camera.FarClip = 300;
            cameraNode.Position = new Vector3(0f, 45f, -20f);
            cameraNode.Rotation = new Quaternion(x: 60, y: 0, z: 0);
            

            Log.Write(LogLevel.Info, string.Format("Camera FOV is {0}", camera.Fov));

            

            // Viewport
            Renderer.SetViewport(1, new Viewport(Context, scene, camera, null));
        }
    }
}
