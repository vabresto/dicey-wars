﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Urho;
using Urho.Gui;
using Urho.Actions;
using Urho.Shapes;
using Urho.Physics;
using Urho.IO;

namespace DicePhysics
{
    public class PhysicalCombat
    {
        private List<Node> nodesList = new List<Node>();
        public bool IsInCombat
        { get; protected set; }
        public bool UserTappedAcceptEnd
        { get; protected set; }
        private bool isDone;

        public bool SimulateRealDice = false;
        private bool _isSimulatingRealDice = false;

        public enum DiceDirection
        {
            One = 1,
            Two = 2,
            Three = 3,
            Four = 4,
            Five = 5,
            Six = 6,
        };

        public Dictionary<DiceDirection, Vector3> diceHeadings = new Dictionary<DiceDirection, Vector3>()
        {
            { DiceDirection.One,    new Vector3(0f,-1f,0f) },   // Good
            { DiceDirection.Two,    new Vector3(0f,0f,1f) },
            { DiceDirection.Three,  new Vector3(1f,0f,0f) },
            { DiceDirection.Four,   new Vector3(-1f,0f,0f) },
            { DiceDirection.Five,   new Vector3(0f,0f,-1f) },
            { DiceDirection.Six,    new Vector3(0f,1f,0f) },    // Good
        };

        Vector3 cameraPositionInitial;
        public int player1Score;
        public int player2Score;

        Hexagon attacker;
        Hexagon defender;

        public void StartCombat(int player1ID, int numDicePlayer1, int player2ID, int numDicePlayer2, Hexagon _attacker, Hexagon _defender)
        {
            player1Score = 0;
            player2Score = 0;
            IsInCombat = true;
            isDone = false;
            UserTappedAcceptEnd = false;
            _isSimulatingRealDice = false;

            attacker = _attacker;
            defender = _defender;

            if (SimulateRealDice)
            {
                _isSimulatingRealDice = true;
                //MyGame.gameManager.isCameraLocked = true;

                //nodesList.Clear();
                //Log.Write(LogLevel.Debug, String.Format("Nodes: {}", nodesList.Count));


                Scene scene = MyGame.scene;
                Random rand = MyGame.rand;

                cameraPositionInitial = scene.GetChild("camera").Position;
                scene.GetChild("camera").Position = new Vector3(0f, 45f, -20f);

                //Make the floor
                Node floor = scene.CreateChild("combat_floor");
                floor.Position = new Vector3(x: 0, y: 10, z: 0);
                floor.Scale = new Vector3(x: 100, y: 1, z: 100);
                StaticModel floorModel = floor.CreateComponent<StaticModel>();
                floorModel.Model = ResourceManager.GetModel(ModelResourceType.Floor);
                RigidBody floorBody = floor.CreateComponent<RigidBody>();
                floorBody.Friction = 0.85f;
                CollisionShape floorShape = floor.CreateComponent<CollisionShape>();
                floorShape.SetBox(floor.Scale, Vector3.Zero, Quaternion.Identity);
                nodesList.Add(floor);

                //Spawn P1's dice
                int numDice = 0;
                for (int cnt = 0; cnt < numDicePlayer1; ++cnt)
                {
                    Node die = scene.CreateChild(name: "Player_" + player1ID + "_Die_" + numDice);
                    ++numDice;
                    die.SetScale(1f);
                    InitializeDiePosition(die);
                    die.AddTag("CombatDice");
                    die.AddTag("Player1");

                    StaticModel boxModel = die.CreateComponent<StaticModel>();
                    boxModel.Model = ResourceManager.GetModel(ModelResourceType.Dice);
                    boxModel.SetMaterial(ResourceManager.GetMaterial(
                        new Tuple<PlayerColour, MaterialResourceType>(PlayerManager.GetPlayer(player1ID).Colour, MaterialResourceType.Dice)));


                    RigidBody boxBody = die.CreateComponent<RigidBody>();
                    boxBody.Mass = 20f;
                    boxBody.SetLinearVelocity(new Vector3(rand.Next() % 6 - 3, rand.Next() % 6 - 3, rand.Next() % 6 - 3));
                    boxBody.SetAngularVelocity(new Vector3(rand.Next() % 10 - 5, rand.Next() % 10 - 5, rand.Next() % 10 - 5));

                    CollisionShape boxShape = die.CreateComponent<CollisionShape>();
                    boxShape.SetBox(new Vector3(2f, 2f, 2f), Vector3.Zero, Quaternion.Identity);
                    nodesList.Add(die);
                }

                //Spawn P2's dice
                numDice = 0;
                for (int cnt = 0; cnt < numDicePlayer2; ++cnt)
                {
                    Node die = scene.CreateChild(name: "Player_" + player2ID + "_Die_" + numDice);
                    ++numDice;
                    die.SetScale(1f);
                    InitializeDiePosition(die);
                    die.AddTag("CombatDice");
                    die.AddTag("Player2");

                    StaticModel boxModel = die.CreateComponent<StaticModel>();
                    boxModel.Model = ResourceManager.GetModel(ModelResourceType.Dice);
                    boxModel.SetMaterial(ResourceManager.GetMaterial(
                        new Tuple<PlayerColour, MaterialResourceType>(PlayerManager.GetPlayer(player2ID).Colour, MaterialResourceType.Dice)));

                    RigidBody boxBody = die.CreateComponent<RigidBody>();
                    boxBody.Mass = 20f;
                    boxBody.SetLinearVelocity(new Vector3(rand.Next() % 6 - 3, rand.Next() % 6 - 3, rand.Next() % 6 - 3));
                    boxBody.SetAngularVelocity(new Vector3(rand.Next() % 10 - 5, rand.Next() % 10 - 5, rand.Next() % 10 - 5));

                    CollisionShape boxShape = die.CreateComponent<CollisionShape>();
                    boxShape.SetBox(new Vector3(2f, 2f, 2f), Vector3.Zero, Quaternion.Identity);
                    nodesList.Add(die);
                }
            }
            else
            {
                for (int cnt = 0; cnt < numDicePlayer1; ++cnt)
                {
                    player1Score += MyGame.rand.Next() % 6 + 1;
                }
                for (int cnt = 0; cnt < numDicePlayer2; ++cnt)
                {
                    player2Score += MyGame.rand.Next() % 6 + 1;
                }
            }
        }

        public void DrawDebug()
        {
            /*
            Scene scene = MyGame.scene;

            foreach (Node item in nodesList)
            {
                if (item.HasTag("CombatDice"))
                {
                    
                    scene.GetComponent<DebugRenderer>().AddLine(item.Position, item.Position + 2 * item.Up, Color.Yellow, false);
                    scene.GetComponent<DebugRenderer>().AddLine(item.Position, item.Position + 2 * item.Direction, Color.Blue, false);
                    scene.GetComponent<DebugRenderer>().AddLine(item.Position, item.Position + 2 * item.Right, Color.Green, false);
                }
            }
            */
        }

        // Poor way of doing this, but w.e
        public int CalcDiceValue(Node dice)
        {
            int ret = 0;
            float closest = 1000000000;
            Vector3 absUp = new Vector3(0f, 1f, 0f);
            Vector3 distSqr;

            distSqr = dice.Up - absUp;
            if (distSqr.LengthSquared < closest)
            {
                closest = distSqr.LengthSquared;
                ret = 6;
            }
            distSqr = -dice.Up - absUp;
            if (distSqr.LengthSquared < closest)
            {
                closest = distSqr.LengthSquared;
                ret = 1;
            }
            distSqr = dice.Direction - absUp;
            if (distSqr.LengthSquared < closest)
            {
                closest = distSqr.LengthSquared;
                ret = 2;
            }
            distSqr = -dice.Direction - absUp;
            if (distSqr.LengthSquared < closest)
            {
                closest = distSqr.LengthSquared;
                ret = 5;
            }
            distSqr = dice.Right - absUp;
            if (distSqr.LengthSquared < closest)
            {
                closest = distSqr.LengthSquared;
                ret = 3;
            }
            distSqr = -dice.Right - absUp;
            if (distSqr.LengthSquared < closest)
            {
                closest = distSqr.LengthSquared;
                ret = 4;
            }

            return ret;
        }

        public void OnTap()
        {
            if (isDone)
            {
                UserTappedAcceptEnd = true;
            }
        }

        public bool DoneCombat()
        {
            isDone = true;

            if (_isSimulatingRealDice)
            {
                player1Score = 0;
                player2Score = 0;

                foreach (Node item in nodesList)
                {
                    if (item == null)
                    {
                        continue;
                    }

                    RigidBody phys = item.GetComponent<RigidBody>();
                    if (phys != null && phys.AngularVelocity != Vector3.Zero && phys.LinearVelocity != Vector3.Zero)
                    {
                        isDone = false;
                    }
                    else
                    {
                        if (item.HasTag("Player1"))
                        {
                            player1Score += CalcDiceValue(item);
                        }
                        else if (item.HasTag("Player2"))
                        {
                            player2Score += CalcDiceValue(item);
                        }
                    }
                }
            }

            return isDone && UserTappedAcceptEnd;
        }

        public void EndCombat()
        {
            if (_isSimulatingRealDice)
            {
                Scene scene = MyGame.scene;
                foreach (Node item in nodesList)
                {
                    scene.RemoveChild(item);
                }
                scene.GetChild("camera").Position = cameraPositionInitial;
                nodesList.Clear();
            }

            IsInCombat = false;

            if (player1Score > player2Score)
            {
                // Attacker wins
                MyGame.hexManager.AssignHex(attacker.OwnerPlayerID, defender);

                MyGame.hexManager.SetCounters(defender, attacker.GetNumberOfCounters() - 1);
                MyGame.hexManager.SetCounters(attacker, 1);
            }
            else
            {
                // Defender wins
                MyGame.hexManager.SetCounters(attacker, 1);
            }
            
            MyGame.hexManager.DeselectCurrent();
        }

        void InitializeDiePosition(Node die)
        {
            Random rand = MyGame.rand;

            if (die != null)
            {
                die.Position = new Vector3(x: rand.Next() % 10 - 5, y: rand.Next() % 8 + 12, z: rand.Next() % 10 - 5);
                //die.Rotation = new Quaternion(x: rand.Next() % 90, y: rand.Next() % 90, z: rand.Next() % 90);
            }
        }
    }
}
