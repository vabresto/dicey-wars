﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Urho;
using Urho.IO;

namespace DicePhysics
{
    public class GameManager
    {
        List<int> playersList = new List<int>();
        int currentActivePlayer;
        int localPlayer;
        public Random rand = new Random();
        public bool isCameraLocked = false;
        public int winnerPlayer = -1;

        public GameManager(int numPlayers)
        {
            currentActivePlayer = 0;

            PlayerManager.AddPlayer(0, PlayerColour.Red);
            playersList.Add(0);
            localPlayer = 0;

            if (numPlayers > 1)
            {
                PlayerManager.AddAIPlayer(1, PlayerColour.Green);
                playersList.Add(1);
            }
            if (numPlayers > 2)
            {
                PlayerManager.AddAIPlayer(2, PlayerColour.Blue);
                playersList.Add(2);
            }
            if (numPlayers > 3)
            {
                PlayerManager.AddAIPlayer(3, PlayerColour.Magenta);
                playersList.Add(3);
            }
        }

        public void NextPlayersTurn()
        {
            MyGame.hexManager.DistributeDiceToPlayer(
                MyGame.gameManager.GetCurrentPlayer(),
                MyGame.hexManager.CalcLongestConnectedChainForPlayer(MyGame.gameManager.GetCurrentPlayer()));

            ++currentActivePlayer;

            if (currentActivePlayer >= playersList.Count)
            {
                currentActivePlayer = 0;
            }

            PlayerManager.PlayerStartTurn();
        }

        public int GetCurrentPlayer()
        {
            return currentActivePlayer;
        }

        public int GetLocalPlayerID()
        {
            return localPlayer;
        }

        public void DoCombat(Hexagon attacker, Hexagon defender)
        {
            if (attacker == null || defender == null)
            {
                return;
            }

            Log.Write(LogLevel.Info, "Combat!");

            if (attacker.GetNumberOfCounters() > 1)
            {
                MyGame.combat.StartCombat(attacker.OwnerPlayerID, attacker.GetNumberOfCounters(), 
                    defender.OwnerPlayerID, defender.GetNumberOfCounters(), 
                    attacker, defender);
            }
        }

        public bool IsGameOver()
        {
            for (int cnt = 0; cnt < MyGame.hexManager.ownedTilesList.Count; ++cnt)
            {
                if (MyGame.hexManager.ownedTilesList[cnt].Count == MyGame.hexManager.NumHexesOnMap)
                {
                    Log.Write(LogLevel.Info, string.Format("Player {0} wins!", cnt));
                    winnerPlayer = cnt;
                    return true;
                }
            }
            return false;
        }
    }
}
