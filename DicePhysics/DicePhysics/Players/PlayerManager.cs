﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DicePhysics.Players;
using DicePhysics.AI;

namespace DicePhysics
{
    class PlayerManager
    {
        private static Dictionary<int, Player> _playersList = new Dictionary<int, Player>();

        PlayerManager() { }

        public static void AddPlayer(int id, PlayerColour colour)
        {
            _playersList.Add(id, new HumanPlayer(colour, id));
        }

        public static void AddAIPlayer(int id, PlayerColour colour)
        {
            _playersList.Add(id, new BasicAI(colour, id));
        }

        public static Player GetPlayer(int id)
        {
            return _playersList[id];
        }

        public static int GetNumPlayers()
        {
            return _playersList.Count;
        }

        public static bool IsPlayerDoneTurn()
        {
            return _playersList[MyGame.gameManager.GetCurrentPlayer()].IsDoneTurn();
        }

        public static void PlayerMakeMove()
        {
            _playersList[MyGame.gameManager.GetCurrentPlayer()].MakeMove();
        }

        public static void PlayerStartTurn()
        {
            _playersList[MyGame.gameManager.GetCurrentPlayer()].StartTurn();
        }

        public static void PlayerOnUpdate(float timeStep)
        {
            _playersList[MyGame.gameManager.GetCurrentPlayer()].OnUpdate(timeStep);
        }

        public static void LocalPlayerEndedTurn()
        {
            if (_playersList[MyGame.gameManager.GetCurrentPlayer()].IsLocalPlayer &&
                _playersList[MyGame.gameManager.GetCurrentPlayer()].IsHuman)
            {
                HumanPlayer player = (HumanPlayer)_playersList[MyGame.gameManager.GetCurrentPlayer()];
                player.HasEndedTurn = true;
            }
        }
    }
}
