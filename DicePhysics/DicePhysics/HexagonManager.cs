﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Urho;
using Urho.Gui;
using Urho.Actions;
using Urho.Shapes;
using Urho.Physics;
using Urho.IO;

namespace DicePhysics
{
    public class HexagonManager
    {
        private const int _HashWidth = 15;

        public Random _rand = new Random();
        private float _timeSinceLastTap = 0f;
        private const float _minTimeBetweenTaps = 1/60f;
        private const float _maxTimeBetweenTaps = 1.5f;
        private int _numTaps = 0;
        public int NumHexesOnMap
        { get; private set; }

        public List<List<Hexagon>> hexesList;
        public List<List<Hexagon>> ownedTilesList;

        Hexagon selected = null;
        Hexagon highlighted = null;

        public HexagonManager(Scene scene, Urho.Resources.ResourceCache cache)
        {
            NumHexesOnMap = 0;
            hexesList = new List<List<Hexagon>>();

            for (int cnt = 0; cnt < _HashWidth; ++cnt)
            {
                hexesList.Add(new List<Hexagon>());
            }

            ownedTilesList = new List<List<Hexagon>>();
            for (int cnt = 0; cnt < PlayerManager.GetNumPlayers(); ++cnt)
            {
                ownedTilesList.Add(new List<Hexagon>());
            }

            // Map gen
            int N = 7;

            for (int dx = -N; dx <= N; ++dx)
            {
                for (int dy = Math.Max(-N, -dx-N); dy <= Math.Min(N, -dx+N); ++dy)
                {
                    if (_rand.Next() % 10 <= 6)
                    {
                        int ownerPlayer = _rand.Next() % PlayerManager.GetNumPlayers();
                        AxialCoord coords = new AxialCoord(dx, dy);

                        InsertHex(new Hexagon(scene, cache, coords, ownerPlayer));
                        ForceAssignHex(ownerPlayer, GetHex(coords));
                        ++NumHexesOnMap;
                    }
                }
            }

            int numDice = _rand.Next() % CalcMostHexesOwnedByAPlayer() * 2 + CalcMostHexesOwnedByAPlayer() * 2;
            for (int cnt = 0; cnt < PlayerManager.GetNumPlayers(); ++cnt)
            {
                DistributeDiceToPlayer(cnt, numDice, true);
            }
        }

        private int Hash(CubeCoord coord)
        {
            return Hash(new AxialCoord(coord));
        }

        private int Hash(AxialCoord coord)
        {
            return (int)Math.Abs((int)(coord.Q - (int)coord.R)) % _HashWidth;
        }

        private void InsertHex(Hexagon hex)
        {
            int hash = Hash(hex.Coord);
            hexesList[hash].Add(hex);
        }

        public Hexagon GetHex(AxialCoord coord)
        {
            int hash = Hash(coord);
            foreach (Hexagon hex in hexesList[hash])
            {
                if (hex.Coord.Q == coord.Q &&
                    hex.Coord.R == coord.R)
                {
                    return hex;
                }
            }

            return null;
        }

        public Hexagon GetHexFromWorldPosition(Vector3 worldPos)
        {
            return GetHexFromPosition2D(new Vector2(worldPos.X, worldPos.Z));
        }

        public Hexagon GetHexFromPosition2D(Vector2 pos2d)
        {
            AxialCoord location = AxialCoord.Round(new AxialCoord(pos2d));

            var hex = GetHex(location);

            if (hex != null)
            {
                return hex;
            }
            return null;
        }

        public List<Hexagon> GetAdjacentHexes(Hexagon hex)
        {
            List<Hexagon> ret = new List<Hexagon>();

            foreach (var direction in AxialCoord.DirectionsList)
            {
                var adjHex = GetHex(hex.Coord + direction.Value);

                if (adjHex != null)
                {
                    ret.Add(adjHex);
                }
            }

            return ret;
        }

        public void OnUpdate(float timeStep)
        {
            _timeSinceLastTap += timeStep;

            if (_timeSinceLastTap >= _maxTimeBetweenTaps)
            {
                _numTaps = 0;
                //highlighted = null;
                //Log.Write(LogLevel.Info, "Reset highlighted!");
            }
        }

        public void OnTap(Vector2 pos)
        {
            if (MyGame.gameManager.GetLocalPlayerID() != MyGame.gameManager.GetCurrentPlayer())
            {
                if (selected != null)
                {
                    selected.SetDefaultMaterial();
                    HighlightNeighbours(selected, false);
                }
                return;
            }

            if (highlighted == null)
            {
                highlighted = GetHexFromPosition2D(new Vector2(pos.X, pos.Y));
                Log.Write(LogLevel.Info, "New highlighted!");
            }
            // Doing deselect
            else if (highlighted == selected)
            {
                if (_timeSinceLastTap >= _minTimeBetweenTaps)
                {
                    _timeSinceLastTap = 0f;
                    ++_numTaps;
                }
                if (_numTaps >= 1)
                {
                    //Deselect old
                    DeselectCurrent();
                }
            }
            // Doing select
            else if (highlighted == GetHexFromPosition2D(new Vector2(pos.X, pos.Y)))
            {
                if (highlighted.OwnerPlayerID == MyGame.gameManager.GetCurrentPlayer())
                {
                    // Current player is selecting
                    // Note: Can be triggered by AI
                    if (_timeSinceLastTap >= _minTimeBetweenTaps)
                    {
                        _timeSinceLastTap = 0f;
                        ++_numTaps;
                        Log.Write(LogLevel.Info, "Tap!");
                    }
                    if (_numTaps >= 1)
                    {
                        //Deselect old
                        if (selected != null)
                        {
                            selected.SetDefaultMaterial();
                            HighlightNeighbours(selected, false);
                        }

                        SelectHex(highlighted);
                        highlighted = null;
                        _numTaps = 0;

                        //Highlight opponents
                        HighlightEnemies(selected);
                    }
                }
                else
                {
                    // Player is trying to attack
                    if (!MyGame.combat.IsInCombat)
                    {
                        MyGame.gameManager.DoCombat(selected, highlighted);
                    }
                    

                    if (highlighted.OwnerPlayerID == MyGame.gameManager.GetCurrentPlayer())
                    {
                        DeselectCurrent();
                    }
                }
            }
            else
            {
                highlighted = GetHexFromPosition2D(new Vector2(pos.X, pos.Y));
                Log.Write(LogLevel.Info, "Selected another hex!");
            }
        }

        // Call before HighlightHex
        public void SelectHex(Hexagon hex)
        {
            DeselectCurrent();
            selected = hex;
            selected.SetSelectedMaterial();
        }

        // Call after SelectHex
        public void HighlightHex(Hexagon hex)
        {
            if (highlighted != null)
            {
                highlighted.SetDefaultMaterial();
            }
            highlighted = hex;
            highlighted.SetHighlightedMaterial();
        }

        public void DeselectCurrent()
        {
            if (selected != null)
            {
                selected.SetDefaultMaterial();
                HighlightNeighbours(selected, false);
            }

            highlighted = null;
            selected = null;
            _numTaps = 0;
        }

        void HighlightEnemies(Hexagon hex)
        {
            foreach (var direction in AxialCoord.DirectionsList)
            {
                var adjHex = GetHex(hex.Coord + direction.Value);

                if (adjHex != null)
                {
                    if (adjHex.OwnerPlayerID != hex.OwnerPlayerID)
                    {
                        adjHex.SetHighlightedMaterial();
                    }
                }
            }
        }

        void HighlightNeighbours(Hexagon hex, bool select)
        {
            foreach(var direction in AxialCoord.DirectionsList)
            {
                var adjHex = GetHex(hex.Coord + direction.Value);

                if (adjHex != null)
                {
                    if (select)
                    {
                        adjHex.SetHighlightedMaterial();
                    }
                    else
                    {
                        adjHex.SetDefaultMaterial();
                    }
                }
            }
        }

        public void AssignHex(int player, Hexagon hex)
        {
            for (int cnt = 0; cnt < ownedTilesList[hex.OwnerPlayerID].Count; ++cnt)
            {
                if (ownedTilesList[hex.OwnerPlayerID][cnt] == hex)
                {
                    ownedTilesList[hex.OwnerPlayerID].RemoveAt(cnt);
                    hex.OwnerPlayerID = player;
                    ownedTilesList[player].Add(hex);
                }
            }
        }

        void ForceAssignHex(int player, Hexagon hex)
        {
            hex.OwnerPlayerID = player;
            ownedTilesList[player].Add(hex);
        }

        public void SetCounters(Hexagon hex, int numCounters)
        {
            int hash = Hash(hex.Coord);
            for (int cnt = 0; cnt < ownedTilesList[hex.OwnerPlayerID].Count; ++cnt)
            {
                if (ownedTilesList[hex.OwnerPlayerID][cnt].Equals(hex))
                {
                    ownedTilesList[hex.OwnerPlayerID][cnt].SetNumberOfCounters(numCounters);
                }
            }
            /*
            for (int cnt = 0; cnt < hexesList[hash].Count; ++cnt)
            {
                if (hexesList[hash][cnt].Equals(hex))
                {
                    hexesList[hash][cnt].SetNumberOfCounters(numCounters);
                }
            }
            */
        }

        public void IncrementCounters(Hexagon hex, int increment)
        {
            int hash = Hash(hex.Coord);
            for (int cnt = 0; cnt < ownedTilesList[hex.OwnerPlayerID].Count; ++cnt)
            {
                if (ownedTilesList[hex.OwnerPlayerID][cnt].Equals(hex))
                {
                    ownedTilesList[hex.OwnerPlayerID][cnt].IncrementCounters(increment);
                }
            }
            /*
            for (int cnt = 0; cnt < hexesList[hash].Count; ++cnt)
            {
                if (hexesList[hash][cnt].Equals(hex))
                {
                    hexesList[hash][cnt].IncrementCounters(increment);
                }
            }
            */
        }

        public int CalcLongestConnectedChainForPlayer(int playerID)
        {
            if (ownedTilesList[playerID].Count <= 0)
            {
                return 0;
            }

            List<Hexagon> uncheckedList = new List<Hexagon>();
            List<Hexagon> tocheckList = new List<Hexagon>();
            List<Hexagon> checkedList = new List<Hexagon>();

            int MaxNumConnected = 0;
            int NumConnected = 0;

            // Mark all as unchecked
            for (int cnt = 0; cnt < ownedTilesList[playerID].Count; ++cnt)
            {
                uncheckedList.Add(ownedTilesList[playerID][cnt]);
            }

            tocheckList.Add(uncheckedList[0]);

            while (tocheckList.Count > 0 || uncheckedList.Count > 0)
            {
                // Handle this 'cluster' first
                if (tocheckList.Count > 0)
                {
                    Hexagon hex = tocheckList[0];

                    foreach (var direction in AxialCoord.DirectionsList)
                    {
                        Hexagon adjHex = GetHex(hex.Coord + direction.Value);

                        if (adjHex != null && !checkedList.Contains(adjHex) 
                            && !tocheckList.Contains(adjHex) && adjHex.OwnerPlayerID == hex.OwnerPlayerID)
                        {
                            tocheckList.Add(adjHex);
                        }
                    }

                    ++NumConnected;
                    checkedList.Add(hex);
                    uncheckedList.Remove(hex);
                    tocheckList.Remove(hex);
                }
                // Current cluster is empty, move on to other clusters
                else
                {
                    MaxNumConnected = Math.Max(MaxNumConnected, NumConnected);
                    NumConnected = 0;
                    tocheckList.Add(uncheckedList[0]);
                    checkedList.Clear();
                }
            }

            MaxNumConnected = Math.Max(MaxNumConnected, NumConnected);

            return MaxNumConnected;
        }

        public int CalcTotalNumberOfDiceOwned(int playerID)
        {
            int ret = 0;

            for (int cnt = 0; cnt < ownedTilesList[playerID].Count; ++cnt)
            {
                ret += ownedTilesList[playerID][cnt].GetNumberOfCounters();
            }

            return ret;
        }

        bool PlayerCanDeployMoreDice(int playerID)
        {
            for (int cnt = 0; cnt < ownedTilesList[playerID].Count; ++cnt)
            {
                if (ownedTilesList[playerID][cnt].GetCounterCapacity() > 0)
                {
                    return true;
                }
            }
            return false;
        }

        List<int> FindHexesWithCapacity(int playerID)
        {
            List<int> ret = new List<int>();
            for (int cnt = 0; cnt < ownedTilesList[playerID].Count; ++cnt)
            {
                if (ownedTilesList[playerID][cnt].GetCounterCapacity() > 0)
                {
                    ret.Add(cnt);
                }
            }
            return ret;
        }

        public void DistributeDiceToPlayer(int playerID, int numDice, bool atLeastOnePerHex = false)
        {
            if (atLeastOnePerHex)
            {
                for (int cnt = 0; cnt < ownedTilesList[playerID].Count; ++cnt)
                {
                    SetCounters(ownedTilesList[playerID][cnt], 1);
                    --numDice;
                }
            }

            List<int> whitelist = FindHexesWithCapacity(playerID);

            while (numDice > 0 && whitelist.Count > 0)
            {
                int inWhitelist = _rand.Next() % whitelist.Count;
                int index = whitelist[inWhitelist];

                if (ownedTilesList[playerID][index].GetCounterCapacity() > 0)
                {
                    IncrementCounters(ownedTilesList[playerID][index], 1);

                   --numDice;
                }
                else
                {
                    whitelist.Remove(index);
                }
            }
        }

        int CalcMostHexesOwnedByAPlayer()
        {
            int num = 0;
            for (int cnt = 0; cnt < PlayerManager.GetNumPlayers(); ++cnt)
            {
                num = Math.Max(num, ownedTilesList[cnt].Count);
            }
            return num;
        }
    }
}
