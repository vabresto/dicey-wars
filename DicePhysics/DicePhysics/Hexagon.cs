﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Urho;
using Urho.Gui;
using Urho.Actions;
using Urho.Shapes;
using Urho.Physics;
using Urho.IO;

namespace DicePhysics
{
    //http://www.redblobgames.com/grids/hexagons

    public enum HexDirection
    {
        Above,
        AboveRight,
        BelowRight,
        Below,
        BelowLeft,
        AboveLeft,
    }

    public class CubeCoord
    {
        #region Statics
        public static Dictionary<HexDirection, CubeCoord> DirectionsList = new Dictionary<HexDirection, CubeCoord>()
        {
            { HexDirection.Above, new CubeCoord(0, +1, -1)},
            { HexDirection.AboveRight, new CubeCoord(+1, 0, -1) },
            { HexDirection.BelowRight, new CubeCoord(+1, -1, 0) },
            { HexDirection.Below, new CubeCoord(0, -1, +1) },
            { HexDirection.BelowLeft, new CubeCoord(-1, 0, +1) },
            { HexDirection.AboveLeft, new CubeCoord(-1, +1, 0) },
        };

        public static float Distance(CubeCoord a, CubeCoord b)
        {
            return (Math.Abs(a.X - b.X) + Math.Abs(a.Y - b.Y) + Math.Abs(a.Z - b.Z)) / 2;
        }

        public static CubeCoord GetNeighbour(CubeCoord coord, HexDirection direction)
        {
            return new CubeCoord(coord + DirectionsList[direction]);
        }

        public static CubeCoord Round(CubeCoord coord)
        {
            float rx = (float)Math.Round(coord.X);
            float ry = (float)Math.Round(coord.Y);
            float rz = (float)Math.Round(coord.Z);

            float x_diff = Math.Abs(rx - coord.X);
            float y_diff = Math.Abs(ry - coord.Y);
            float z_diff = Math.Abs(rz - coord.Z);

            if (x_diff > y_diff && x_diff > z_diff)
            {
                rx = -ry - rz;
            }
            else if (y_diff > z_diff)
            {
                ry = -rx - rz;
            }
            else
            {
                rz = -rx - ry;
            }

            return new CubeCoord(rx, ry, rz);
        }
        #endregion

        #region OperatorOverloads
        public static CubeCoord operator+ (CubeCoord a, CubeCoord b)
        {
            return new CubeCoord(a.X + b.X, a.Y + b.Y, a.Z + b.Z);
        }
        public static CubeCoord operator- (CubeCoord a, CubeCoord b)
        {
            return new CubeCoord(a.X - b.X, a.Y - b.Y, a.Z - b.Z);
        }
        public static bool operator == (CubeCoord a, AxialCoord b)
        {
            return a.X == b.Q && a.Z == b.R;
        }
        public static bool operator!=(CubeCoord a, AxialCoord b)
        {
            return !(a == b);
        }
        public static bool operator ==(AxialCoord a, CubeCoord b)
        {
            return b == a;
        }
        public static bool operator !=(AxialCoord a, CubeCoord b)
        {
            return !(a == b);
        }
        #endregion

        #region Constructors
        public CubeCoord(float x, float y, float z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public CubeCoord(CubeCoord coord)
        {
            X = coord.X;
            Y = coord.Y;
            Z = coord.Z;
        }

        public CubeCoord(AxialCoord coord)
        {
            X = coord.Q;
            Z = coord.R;
            Y = -coord.Q - coord.R;
        }
        #endregion

        #region Properties
        public float X { get; set; }
        public float Y { get; set; }
        public float Z { get; set; }
        #endregion

        #region Methods
        public Vector2 GetMapPosition()
        {
            if (X < 0)
            {
                // Works
                if (X % 2 == 1)
                {
                    return new Vector2(X * Hexagon.HorizontalDistance,
                   Z * Hexagon.VerticalDistance + X / 2 * Hexagon.VerticalDistance);
                }
                else
                {
                    return new Vector2(X * Hexagon.HorizontalDistance,
                   Z * Hexagon.VerticalDistance + (X + Hexagon.Height) / 2 * Hexagon.VerticalDistance);
                }
            }
            else
            {
                float fixedOffset = Hexagon._size * Hexagon.HorizontalDistance;

                return new Vector2(X * Hexagon.HorizontalDistance,
                Z * Hexagon.VerticalDistance + X / 2 * Hexagon.VerticalDistance + fixedOffset);
            }
        }
        public Vector3 GetWorldPosition()
        {
            return new Vector3();
        }
        #endregion
    }

    public class AxialCoord
    {
        #region Statics
        public static Dictionary<HexDirection, AxialCoord> DirectionsList = new Dictionary<HexDirection, AxialCoord>()
        {
            { HexDirection.Above, new AxialCoord(0, -1)},
            { HexDirection.AboveRight, new AxialCoord(+1, -1) },
            { HexDirection.BelowRight, new AxialCoord(+1, 0) },
            { HexDirection.Below, new AxialCoord(0, +1) },
            { HexDirection.BelowLeft, new AxialCoord(-1, +1) },
            { HexDirection.AboveLeft, new AxialCoord(-1, 0) },
        };

        public static float Distance(AxialCoord a, AxialCoord b)
        {
            return CubeCoord.Distance(new CubeCoord(a), new CubeCoord(b));
        }

        public static AxialCoord GetNeighbour(AxialCoord coord, HexDirection direction)
        {
            return coord + DirectionsList[direction];
        }

        public static AxialCoord Round(AxialCoord coord)    //Super inefficient
        {
            return new AxialCoord(CubeCoord.Round(new CubeCoord(coord)));
        }
        #endregion

        #region OperatorOverloads
        public static AxialCoord operator +(AxialCoord a, AxialCoord b)
        {
            return new AxialCoord(a.Q + b.Q, a.R + b.R);
        }
        public static AxialCoord operator -(AxialCoord a, AxialCoord b)
        {
            return new AxialCoord(a.Q - b.Q, a.R - b.R);
        }
        #endregion

        #region Constructors
        public AxialCoord(float x, float y)
        {
            Q = x;
            R = y;
        }

        public AxialCoord(CubeCoord coord)
        {
            Q = coord.X;
            R = coord.Z;
        }

        public AxialCoord(AxialCoord coord)
        {
            Q = coord.Q;
            R = coord.R;
        }

        public AxialCoord(Vector2 mapPos)
        {
            //Almost
            #region oldcode
            /*
            if (mapPos.X < 0)
            {
                if (mapPos.X % 2 == 1)
                {
                    Q = (float)Math.Round(mapPos.X / Hexagon.HorizontalDistance);
                    R = (float)Math.Round(mapPos.Y / Hexagon.VerticalDistance - mapPos.X / (2 * Hexagon.VerticalDistance));
                }
                else
                {
                    Q = (float)Math.Round(mapPos.X / Hexagon.HorizontalDistance);
                    R = (float)Math.Round(mapPos.Y / Hexagon.VerticalDistance - (mapPos.X + Hexagon.Height) / (2 * Hexagon.VerticalDistance));
                }
            }
            else
            {
                float fixedOffset = 2 * Hexagon.HorizontalDistance;

                Q = (float)Math.Round(mapPos.X / Hexagon.HorizontalDistance);
                R = (float)Math.Round(mapPos.Y / Hexagon.VerticalDistance - mapPos.X / (2 * Hexagon.VerticalDistance) + fixedOffset);
            }
            */
            #endregion

            Q = mapPos.X * 2 / 3 / Hexagon._size;
            R = (-mapPos.X / 3 + (float)Math.Sqrt(3) / 3 * mapPos.Y) / Hexagon._size;
            R -= Hexagon._size;     //Adjustment needed because we are always off ... not sure why
        }
        #endregion

        #region Properties
        public float Q { get; set; }
        public float R { get; set; }
        #endregion

        #region Methods
        public Vector2 GetMapPosition()
        {
            return new CubeCoord(this).GetMapPosition();
        }
        public Vector3 GetWorldPosition()
        {
            Vector2 mapPos = GetMapPosition();
            return new Vector3(mapPos.X, 1f, mapPos.Y);
        }
        #endregion
    }

    public class Hexagon
    {
        #region Statics
        private static int _NumHexes = 0;

        private const bool _isFlatTopped = true;
        public const float _size = 3f;
        public const float Width = _size * 2f;
        public const float HorizontalDistance = Width * 0.75f;
        public const float Height = 1.73205080757f * _size;     // sqrt(3) / 2 * Width
        public const float VerticalDistance = Height;

        public const int maxNumDicePerHex = 9;
        #endregion

        #region Constructors
        public Hexagon(Scene scene, Urho.Resources.ResourceCache cache, AxialCoord coord, int playerID)
        {
            Debug.Assert(_isFlatTopped);

            Log.Write(LogLevel.Debug, "Creating hexagon (Axial)...");

            Obj = scene.CreateChild(name: "Hexagon" + _NumHexes);
            ++_NumHexes;
            Obj.SetScale(_size);
            Obj.Position = coord.GetWorldPosition();
            Obj.Rotation = new Quaternion(x: 0f, y: _isFlatTopped ? 90f : 0f, z: -90f);

            Obj.AddTag("Selectable");
            Obj.AddTag("Hex");

            Coord = coord;
            OwnerPlayerID = playerID;

            StaticModel boxModel = Obj.CreateComponent<StaticModel>();
            boxModel.Model = cache.GetModel("Models/Hexagon.mdl");

            SetDefaultMaterial();

            //SetNumberOfCounters(1);
        }
        #endregion


        public override bool Equals(object obj)
        {
            var rhs = obj as Hexagon;

            if (rhs == null)
            {
                return false;
            }

            return Coord.Q == rhs.Coord.Q && Coord.R == rhs.Coord.R;
        }

        #region Properties
        public Node Obj
        { get; set; }
        public AxialCoord Coord
        { get; set; }

        public int OwnerPlayerID
        { get; set; }

        public List<Counter> Counters = new List<Counter>();
        #endregion

        public void SetDefaultMaterial()
        {
            Obj.GetComponent<StaticModel>().SetMaterial(ResourceManager.GetMaterial(
                    new Tuple<PlayerColour, MaterialResourceType>(
                        PlayerManager.GetPlayer(OwnerPlayerID).Colour,
                        MaterialResourceType.Default)));
        }

        public void SetSelectedMaterial()
        {
            Obj.GetComponent<StaticModel>().SetMaterial(ResourceManager.GetMaterial(
                new Tuple<PlayerColour, MaterialResourceType>(
                    PlayerManager.GetPlayer(OwnerPlayerID).Colour,
                    MaterialResourceType.Select)));
        }

        public void SetHighlightedMaterial()
        {
            Obj.GetComponent<StaticModel>().SetMaterial(ResourceManager.GetMaterial(
                new Tuple<PlayerColour, MaterialResourceType>(
                    PlayerManager.GetPlayer(OwnerPlayerID).Colour,
                    MaterialResourceType.Highlight)));
        }

        public void SetNumberOfCounters(int num)
        {
            ResetCounters();
            Counters.Clear();

            //Debug.Assert(maxNumDicePerHex >= num);
            if (num > maxNumDicePerHex)
            {
                Log.Write(LogLevel.Error, "A hex was assigned too many dice!");
            }

            num = Math.Min(num, maxNumDicePerHex);

            for (int cnt = 0; cnt < num; ++cnt)
            {
                Vector3 pos = new Vector3(Obj.Position.X - 0.5f, Obj.Position.Y, Obj.Position.Z);

                pos.X += cnt % 2 == 0 ? 0 : 1;
                pos.Y += 0.8f + (cnt % 2 == 0 ? cnt / 2 : (cnt - 1) / 2);
                pos.Z += cnt % 2 == 0 ? 0 : 0.25f;

                Counters.Add(new Counter(MyGame.scene, PlayerManager.GetPlayer(OwnerPlayerID).Colour, pos));
            }
        }

        public int GetNumberOfCounters()
        {
            return Counters.Count;
        }

        public int GetCounterCapacity()
        {
            return maxNumDicePerHex - (Counters.Count + 1);
        }

        public void IncrementCounters(int increment)
        {
            SetNumberOfCounters(Counters.Count + increment);
        }

        void ResetCounters()
        {
            foreach (Counter item in Counters)
            {
                MyGame.scene.RemoveChild(item.Obj);
            }
        }
    }

    public class Counter
    {
        public Node Obj { get; set; }

        public Counter(Scene scene, PlayerColour colour, Vector3 position)
        {
            Obj = scene.CreateChild();
            Obj.SetScale(1f);
            Obj.Position = position;

            StaticModel boxModel = Obj.CreateComponent<StaticModel>();
            boxModel.Model = ResourceManager.GetModel(ModelResourceType.Token);
            boxModel.SetMaterial(ResourceManager.GetMaterial(new Tuple<PlayerColour, MaterialResourceType>(colour, MaterialResourceType.Token)));
        }
    }
}
