﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Urho.IO;

namespace DicePhysics.AI
{
    class BasicAI : AIAgentBase
    {
        const int movesToMakePerTurn = 1;
        int movesLeft = 0;

        const float minTimeBetweenMoves = 0f;
        float timeSinceLastMove = 0f;

        List<Hexagon> actionableHexes = new List<Hexagon>();

        public BasicAI(PlayerColour colour, int id)
            : base(colour, id)
        { }

        public override void StartTurn()
        {
            actionableHexes.Clear();
            movesLeft = movesToMakePerTurn;
            timeSinceLastMove = 0f;
        }

        public override void OnUpdate(float timeStep)
        {
            timeSinceLastMove += timeStep;
        }

        public override bool WantsToMakeMove()
        {
            if (actionableHexes.Count > 0)
            {
                return true;
            }

            foreach (Hexagon hex in MyGame.hexManager.ownedTilesList[PlayerID])
            {
                List<Hexagon> adjs = MyGame.hexManager.GetAdjacentHexes(hex);
                bool hasAdjacentFullHex = false;

                foreach (Hexagon adjacent in adjs)
                {
                    if (adjacent.OwnerPlayerID == hex.OwnerPlayerID
                        && hex.GetNumberOfCounters() == Hexagon.maxNumDicePerHex - 1)
                    {
                        hasAdjacentFullHex = true;
                        break;
                    }
                }

                foreach (Hexagon adjacent in adjs)
                {
                    if (adjacent.OwnerPlayerID != hex.OwnerPlayerID
                        && adjacent.GetNumberOfCounters() < hex.GetNumberOfCounters())
                    {
                        actionableHexes.Add(hex);
                        break;
                    }
                    else if (adjacent.OwnerPlayerID != hex.OwnerPlayerID
                        && hex.GetNumberOfCounters() == Hexagon.maxNumDicePerHex-1
                        && hasAdjacentFullHex)
                    {
                        actionableHexes.Add(hex);
                        break;
                    }
                }
            }
            return actionableHexes.Count > 0;
        }

        public override void MakeMove()
        {
            if (timeSinceLastMove < minTimeBetweenMoves)
            {
                return;
            }

            /*
            foreach (Hexagon hex in MyGame.hexManager.ownedTilesList[PlayerID])
            {
                List<Hexagon> adjs = MyGame.hexManager.GetAdjacentHexes(hex);

                foreach (Hexagon adjacent in adjs)
                {
                    if (adjacent.OwnerPlayerID != hex.OwnerPlayerID 
                        && adjacent.GetNumberOfCounters() < hex.GetNumberOfCounters())
                    {
                        MyGame.hexManager.SelectHex(hex);
                        MyGame.hexManager.HighlightHex(adjacent);

                        MyGame.FocusCamera(hex.Obj.Position);

                        MyGame.gameManager.DoCombat(hex, adjacent);
                        --movesLeft;
                        timeSinceLastMove = 0f;
                        return;
                    }
                }
            }
            */

            bool madeAMove = false;
            if (WantsToMakeMove())
            {
                for (int cnt = 0; cnt < actionableHexes.Count; ++cnt)
                {
                    Hexagon hex = actionableHexes[cnt];
                    List<Hexagon> adjs = MyGame.hexManager.GetAdjacentHexes(hex);

                    foreach (Hexagon adjacent in adjs)
                    {
                        if (adjacent.OwnerPlayerID != hex.OwnerPlayerID
                            && adjacent.GetNumberOfCounters() <= hex.GetNumberOfCounters()
                            && !MyGame.combat.IsInCombat)
                        {
                            MyGame.hexManager.SelectHex(hex);
                            MyGame.hexManager.HighlightHex(adjacent);

                            MyGame.FocusCamera(hex.Obj.Position);

                            MyGame.gameManager.DoCombat(hex, adjacent);
                            timeSinceLastMove = 0f;

                            actionableHexes.Remove(hex);
                            madeAMove = true;
                            --cnt;
                            break;
                        }
                    }
                }
            }

            if (!madeAMove)
            {
                actionableHexes.Clear();
            }
        }

        public override bool IsDoneTurn()
        {
            if (!WantsToMakeMove())
            {
                Log.Write(Urho.LogLevel.Info, string.Format("BasicAI {0} ended turn.", PlayerID));
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
