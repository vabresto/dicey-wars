﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Urho;
using Urho.Gui;
using Urho.Actions;
using Urho.Shapes;
using Urho.Physics;
using Urho.IO;

namespace DicePhysics
{
    class Player
    {
        public Player(PlayerColour colour, int id)
        {
            Colour = colour;
            PlayerID = id;
            IsHuman = false;
        }

        public PlayerColour Colour
        { get; private set; }

        public bool IsLocalPlayer
        { get; protected set; }

        public int PlayerID
        { get; private set; }

        public bool IsHuman
        { get; protected set; }

        public virtual void StartTurn()
        { }

        public virtual bool WantsToMakeMove()
        { return false; }

        public virtual void MakeMove()
        { }

        public virtual bool IsDoneTurn()
        { return true; }

        public virtual void OnUpdate(float timeStep)
        { }
    }
}
