﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DicePhysics.Players
{
    sealed class HumanPlayer : Player
    {
        public bool HasEndedTurn
        { get; set; }

        public HumanPlayer(PlayerColour colour, int id)
            : base(colour, id)
        {
            IsLocalPlayer = true;
            IsHuman = true;
        }

        public override void StartTurn()
        {
            HasEndedTurn = false;
        }

        public override bool IsDoneTurn() 
        { return HasEndedTurn; }
    }
}
